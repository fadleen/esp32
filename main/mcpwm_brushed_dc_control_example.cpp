/* brushed dc motor control example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

/*
 * This example will show you how to use MCPWM module to control brushed dc motor.
 * This code is tested with L298 motor driver.
 * User may need to make changes according to the motor driver they use.
*/

//https://howtomechatronics.com/tutorials/arduino/arduino-dc-motor-control-tutorial-l298n-pwm-h-bridge/
//https://github.com/LilyGO/TTGO-T8-ESP32




#include <stdio.h>
#include <iostream>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_attr.h"

#include "driver/mcpwm.h"
#include "soc/mcpwm_reg.h"
#include "soc/mcpwm_struct.h"

#include "driver/gpio.h"	//needed for motor directional ctr for L298N

#include "Header.h"

#define GPIO_PWM0A_OUT 18   //Set GPIO 15 as PWM0A
#define GPIO_PWM0B_OUT 2   //Set GPIO 16 as PWM0B

//ESP32 IO pins to define the dir and speed of left motor
#define GPIO_R_1 2
#define GPIO_R_2 4
#define GPIO_R_PWM 5
#define GPIO_R_CTRL_PINS ((1ULL << GPIO_R_1) | (1ULL << GPIO_R_2))

//ESP32 IO pins to define the dir and speed of right motor
#define GPIO_L_1 18
#define GPIO_L_2 19
#define GPIO_L_PWM 22
#define GPIO_L_CTRL_PINS ((1ULL << GPIO_L_1) | (1ULL << GPIO_L_2))


void setup_hbridge_gpio()
{
	std::cout << "Start Setup L298N left motor and right motor input control";
	
	//setup gpio structure for left motor
	gpio_config_t motorLeft_t;
	motorLeft_t.mode = GPIO_MODE_OUTPUT;
	motorLeft_t.intr_type = GPIO_INTR_DISABLE;
	//motorLeft_t.pin_bit_mask = (GPIO_SEL_2 | GPIO_SEL_4);		//will this work?
	motorLeft_t.pin_bit_mask = GPIO_R_CTRL_PINS;				//should be equivqlent to above
	gpio_config(&motorLeft_t);
	
	//setup gpio structure for right motor
	gpio_config_t motorRight_t;
	motorRight_t.mode = GPIO_MODE_OUTPUT;
	motorRight_t.intr_type = GPIO_INTR_DISABLE;
	//motorRight_t.pin_bit_mask = (GPIO_SEL_18 | GPIO_SEL_19);		//will this work?
	motorRight_t.pin_bit_mask = GPIO_L_CTRL_PINS;  				//should be equivqlent to above
	gpio_config(&motorRight_t);
	
	std::cout << "Completed Setup L298N left motor and right motor input control";
}



static void mcpwm_example_gpio_initialize()
{
    printf("initializing mcpwm gpio...\n");
	mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0A, GPIO_R_PWM);				//i want to use MCPWM0A only.
    //mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0B, GPIO_R_PWM);			//possible if not used? Yes it is ok.
	
	//set up structure for GPIO_IN_1 and GPIO_IN_2 
	//gpio_config_t ctrlPin;
	std::cout << (func()) << std::endl;			//just to test		
	
	
}

/**
 * @brief motor moves in forward direction, with duty cycle = duty %
 */
static void brushed_motor_forward(mcpwm_unit_t mcpwm_num, mcpwm_timer_t timer_num , float duty_cycle)
{
    mcpwm_set_signal_low(mcpwm_num, timer_num, MCPWM_OPR_B);
    mcpwm_set_duty(mcpwm_num, timer_num, MCPWM_OPR_A, duty_cycle);
    mcpwm_set_duty_type(mcpwm_num, timer_num, MCPWM_OPR_A, MCPWM_DUTY_MODE_0); //call this each time, if operator was previously in low/high state
}

/**
 * @brief motor moves in backward direction, with duty cycle = duty %
 */
static void brushed_motor_backward(mcpwm_unit_t mcpwm_num, mcpwm_timer_t timer_num , float duty_cycle)
{
    mcpwm_set_signal_low(mcpwm_num, timer_num, MCPWM_OPR_A);
    mcpwm_set_duty(mcpwm_num, timer_num, MCPWM_OPR_B, duty_cycle);
    mcpwm_set_duty_type(mcpwm_num, timer_num, MCPWM_OPR_B, MCPWM_DUTY_MODE_0);  //call this each time, if operator was previously in low/high state
}

/**
 * @brief motor stop
 */
static void brushed_motor_stop(mcpwm_unit_t mcpwm_num, mcpwm_timer_t timer_num)
{
    mcpwm_set_signal_low(mcpwm_num, timer_num, MCPWM_OPR_A);
    mcpwm_set_signal_low(mcpwm_num, timer_num, MCPWM_OPR_B);
}

/**
 * @brief Configure MCPWM module for brushed dc motor
 */
static void mcpwm_example_brushed_motor_control(void *arg)
{
	

    //1. mcpwm gpio initialization
    mcpwm_example_gpio_initialize();

    //2. initial mcpwm configuration
    printf("Configuring Initial Parameters of mcpwm...\n");
    mcpwm_config_t pwm_config;
    pwm_config.frequency = 500;    //frequency = 500Hz,   was 1000
    pwm_config.cmpr_a = 50.0;    //set duty cycle to 50.0%
    pwm_config.cmpr_b = 25;    //duty cycle of PWMxb = 0				//
    pwm_config.counter_mode = MCPWM_UP_COUNTER;
    pwm_config.duty_mode = MCPWM_DUTY_MODE_0;
    mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &pwm_config);    //Configure PWM0A & PWM0B with above settings
    while (1) {
        brushed_motor_forward(MCPWM_UNIT_0, MCPWM_TIMER_0, 50.0);
        vTaskDelay(2000 / portTICK_RATE_MS);
        brushed_motor_backward(MCPWM_UNIT_0, MCPWM_TIMER_0, 30.0);
        vTaskDelay(2000 / portTICK_RATE_MS);
        brushed_motor_stop(MCPWM_UNIT_0, MCPWM_TIMER_0);
        vTaskDelay(2000 / portTICK_RATE_MS);
    }
}

extern "C" void app_main()
{
	//test SPIRAM usage
	int *someptr = NULL;
	someptr = (int *)malloc(sizeof(int) * 8);
	if (someptr)
	{
		
		printf("Memory allocated..");
	}
	free(someptr);
	//-----------------------
	
	//static DRAM_ATTR int q = 123456;
	
	
	esp_err_t ret;
	setup_hbridge_gpio(); 							//1. set up IN1 and IN2 GPIOs
	
	mcpwm_example_gpio_initialize();				//2. set up gpio to output PWM for left motor
	
	
	mcpwm_config_t pwm_L_config;					//3. setup and config PWM for L motor
	pwm_L_config.frequency = 500;					//freq=100Hz
	pwm_L_config.cmpr_a = 25;						//25% duty cycle
	pwm_L_config.cmpr_b = 0.0;						//not used so duty cycle set to 0%
	pwm_L_config.counter_mode = MCPWM_UP_COUNTER;
	pwm_L_config.duty_mode = MCPWM_DUTY_MODE_0;
	mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &pwm_L_config);		//PWM will start sourcing here
	
	//Start PWM?
	//mcpwm_set_signal_low(MCPWM_UNIT_0,MCPWM_TIMER_0, MCPWM_OPR_A);
	//::mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_OPR_A, 75.0);
	//mcpwm_set_duty_type(MCPWM_UNIT_0,MCPWM_TIMER_0,MCPWM_OPR_A,MCPWM_DUTY_MODE_0);
	
	
	//motor spi spins with 3.3V from ESP32 but prefably  need to xlate to 5V with TXB0108
	//not necessarily needed to interface with a voltage translator.
	ret = ::gpio_set_level((gpio_num_t)GPIO_R_1, 0);			//IN1 = 0	//stop
	ret = ::gpio_set_level((gpio_num_t)GPIO_R_2, 0);			//IN2 = 0
	
	ret = ::gpio_set_level((gpio_num_t)GPIO_R_1, 1);			//IN1 = 1	//spin forward
																//IN2 = 0
	
	ret =::gpio_set_level((gpio_num_t)GPIO_R_1, 0);				//IN1 = 0
	ret =::gpio_set_level((gpio_num_t)GPIO_R_2, 1);				//In2 = 1	//spin backwards
	
																//IN1 = 0	//stop
	ret =::gpio_set_level((gpio_num_t)GPIO_R_2, 0); 			//IN2 = 0
		
	//test end
	
    printf("Testing brushed motor...\n");
    //xTaskCreate(mcpwm_example_brushed_motor_control, "mcpwm_example_brushed_motor_control", 4096, NULL, 5, NULL);
}