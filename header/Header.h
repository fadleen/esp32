#pragma once
#include <driver/gpio.h>
#include <driver/mcpwm.h>

#ifdef __cplusplus
extern "C"
{
#endif
	//add function prorotypes here
	int func();
	void setup_hbridge_gpio();
	void spinWheels(gpio_num_t portNum, mcpwm_config_t pwmConfig);				//interface to spin motor.need to determine args.
	
#ifdef __cplusplus
}
#endif